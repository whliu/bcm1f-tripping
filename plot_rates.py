import matplotlib.pyplot as plt
import reprocess_utils as utils
import pandas as pd 
import tables as t
from os import listdir
import plotly.express as px
import sys
import numpy as np
import os
from datetime import datetime
import pytz
from typing import Dict, List


pd.options.plotting.backend = "plotly"  # for whenever plotly is available 
convert = lambda x: datetime.fromtimestamp(int(x), pytz.timezone("Europe/Stockholm"))

all_channels = np.arange(1, 49)
fill = int(sys.argv[1])
excluded_channels = [8, 9, 10, 11, 12, 24, 41, 42, 43, 44, 45, 46, 47, 48]  # There is no sigma_vis data for these channels
channel_mask = [ch for ch in all_channels if ch not in excluded_channels]

# Load sigma_vis data which is used to weight the mu values
sig_vis_perChannel = pd.read_csv('updated_sigvis.csv')
sig_vis_perChannel = sig_vis_perChannel.set_index('channelid')
sig_vis_perChannel = sig_vis_perChannel.to_dict()['sig_vis']

# Calculate average sigma_vis
sigvis_avg = 0
for channel in channel_mask:
    sigvis_avg += 1 / sig_vis_perChannel[channel]
sigvis_avg = 1 / (sigvis_avg / len(channel_mask))


def read_file(filename, bxmask, query):
    print("============================================", filename.split("/")[-1])
    try:
        h5in = t.open_file(filename, mode='r')
        intable = h5in.root.bcm1futca_agg_hist
    except:
        print("File does not contain agghist data - skip")
        return None, False

    lastnb = -1

    output = pd.DataFrame()
    output['timestampsec'] = []

    for ch in channel_mask:
        output['channel_rate_' + str(ch)] = []

    '''
    Each row corresponds to a 4 NB of data for each channel, so
    NB 0, CH1
    NB 0, CH2
        ...
    NB 0, CH48,
    NB 4, CH1,
    NB 4, CH2,
        ...
    '''
    for i, rowin in enumerate(intable.where(query)):
        newrow = {}

        if lastnb == -1:
            lastnb = rowin['nbnum']
            agghist_list = []

        if rowin['nbnum'] != lastnb or i == len(intable) - 1:
            # End of 4 NB reached
            lastnb = rowin['nbnum']

            for agghist, channel_id in zip(agghist_list, channel_mask):
                # Extract mu for this channel only, using zero counting on 4NB of data
                r0 = 1 - (agghist / 2 ** 14)
                r0[r0 < 0] = 0
                mu = -np.log(r0)

                # Mask out non-collidable bunches and add them up
                mu = (mu * bxmask).sum()

                # Scaling according to sigma_vis
                mu_scaled = mu * sigvis_avg / sig_vis_perChannel[channel_id]

                newrow['channel_rate_' + str(channel_id)] = mu_scaled

            newrow['timestampsec'] = rowin['timestampsec']
            output = pd.concat([output, pd.DataFrame([newrow])], axis=0)

            # Reset agghist_list
            agghist_list = []

        agghist_list.append(rowin['agghist'])

    return output, False


def read_allFiles(original_path):
    files = os.listdir(original_path)

    # Load bxmask, which is used to mask out non-collidable bunches
    collTable_file = int(len(files) / 2)
    while collTable_file < len(files):
        try:
            h5 = t.open_file(original_path + files[collTable_file], mode='r')
            bxmask = h5.root.beam[0]['collidable']
            h5.close()
            break
        except:
            print("Problem with beam table - trying next file")
            collTable_file += 1

    # Query only data from channels in channel_mask
    query = ''.join(['(channelid == ' + str(ch) + ') | ' for ch in channel_mask])
    query = '(' + query[:-3] + ') & (algoid == 101)'

    output = pd.DataFrame()

    # The agghist data is spread out over multiple files
    for file in files:
        output_tmp, finished = read_file(original_path + file, bxmask, query)
        output = pd.concat([output, output_tmp], axis=0)
        if finished:
            break

    return output

def get_vdm_query(fill: int, scan_dict: Dict[str, List[List[int]]]) -> str:
    mask_elemements = scan_dict[f"{fill}"]
    if len(mask_elemements) < 1:
        return "timestampsec > 0"
    
    element = mask_elemements[0]
    query = f"((timestampsec >= {element[0]}) & (timestampsec <= {element[1]}))"
    for element in mask_elemements[1:]:
        query += f" | ((timestampsec >= {element[0]}) & (timestampsec <= {element[1]}))"
    return f"~({query})"


# python plotting_channel_rates.py <fill>

if __name__ == '__main__':
    # Channel mask counting from 1 (BRILDAQ channelid selection)
    all_channels = np.arange(1,49)

    #excluded_channels = [8, 9, 10, 11, 12, 24, 41, 42, 43, 44, 45, 46, 47, 48] # Include 12 later - There is no sigma_vis data for these channels
    excluded_channels = [1, 8, 9, 10, 11, 12, 24, 41, 42, 43, 44, 45, 46, 47, 48] # exclude ch 1 in 2024 as its super high on rates
    channel_mask = [ch for ch in all_channels if ch not in excluded_channels]
    
    year = utils.get_year(fill)
    original_path = '/brildata/' + str(year) + '/' + str(fill) + '/'
    plot_data = pd.DataFrame()

    data = read_allFiles(original_path)
    data['timestampsec'] = data['timestampsec'].astype(int)
    data['timestampsec'] = data['timestampsec'].apply(utils.convert)
    data.set_index('timestampsec', inplace=True)

    # Plot the channel rates
    for ch in channel_mask:
        plot_data['channel ' + str(ch)] = data['channel_rate_' + str(ch)]

    fig = plot_data.plot(title=f'Channel rates for fill {fill}')
    fig.update_layout(showlegend=True)
    fig.show()
    outputPath = f'./plots/24_rates/channel_rates_{fill}.html'#f'./output/trips/channel_rates_{fill}.html'
    print("Saving to ", outputPath)
    fig.write_html(outputPath)

