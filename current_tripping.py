
import pathlib
import pandas
import plotly.express as px
import matplotlib.pyplot as plt
import sys
import pandas as pd
import pytz
from datetime import datetime

# https://twiki.cern.ch/twiki/bin/view/CMS/BcmDocumentation#BCM1F_CAEN_Channel_allocation_in
#BCM1F_CHANNEL_MAP = dict(zip(range(0, 48, 2), [f'"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Z{q[0]}/1FZ{q}Ch{ch:02}"' for q in ['pNe', 'pFa', 'mNe', 'mFa'] for ch in range(6)])) 
#BCM1F_CHANNEL_MAP = BCM1F_CHANNEL_MAP | {k+1: v for k, v in BCM1F_CHANNEL_MAP.items()}

BCM1F_CHANNEL_MAP = {    
    1: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh04"',
    2: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh04"',
    3: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh05"',
    4: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh05"',
    5: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh02"',
    6: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh02"',
    7: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh03"',
    8: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh03"',
    9: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh00"',
    10: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh00"',
    11: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh01"',
    12: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpNeCh01"',
    13: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh04"',
    14: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh04"',
    15: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh05"',
    16: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh05"',
    17: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh02"',
    18: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh02"',
    19: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh03"',
    20: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh03"',
    21: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh00"',
    22: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh00"',
    23: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh01"',
    24: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zp/1FZpFaCh01"',
    25: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh04"',
    26: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh04"',
    27: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh05"',
    28: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh05"',
    29: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh03"',
    30: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh03"',
    31: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh02"',
    32: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh02"',
    33: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh00"',
    34: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh00"',
    35: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh01"',
    36: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmNeCh01"',
    37: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh04"',
    38: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh04"',
    39: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh05"',
    40: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh05"',
    41: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh02"',
    42: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh02"',
    43: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh03"',
    44: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh03"',
    45: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh00"',
    46: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh00"',
    47: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh01"',
    48: '"CMS_BRIL/CMS_BRIL_Essentiel/BCMF/HV_BCMF1/HV_BCMF1_Zm/1FZmFaCh01"',
}


# https://twiki.cern.ch/twiki/bin/viewauth/CMS/PLT#PLT_Channel_Map
PLT_CHANNEL_MAP = dict(zip(range(16), [f'"CMS_PLT/PLT_H{q[0]}/PLT_H{q}/PLTHV_H{q}T{ch}"' for q in ['mN','mF','pN','pF'] for ch in [0,2,1,3]])) 

convert = lambda x: datetime.fromtimestamp(int(x), pytz.timezone("Europe/Stockholm"))


def parse_channel(log_file: str, ch: str, ch_map: dict[int, str]) -> pandas.DataFrame:
    log = pathlib.Path(log_file).read_text(encoding='utf-8-sig').splitlines() # utf-8-sig encoding removes byte order mark ('\ufeff') [https://stackoverflow.com/a/49150749/13019084]
    head = log.index(ch_map.get(ch))
    tail = log.index('', head)
    assert tail == head + log[head:].index('')
    ch_log = pandas.Series(log[head+2 : tail]).rename(f'ch_{ch}')
    ch_log = ch_log.str.replace('"', '')
    ch_log = ch_log.str.split(',', expand=True)
    ch_log.columns = log[head+1].split(',')
    ch_log = ch_log.astype(float)
    #ch_log['Date'] = pandas.to_datetime(ch_log.Date, unit='ms', utc=True)
    ch_log['Date'] = ch_log['Date']/1000 # convert to seconds
    ch_log['Date'] = ch_log['Date'].apply(convert)
    return ch_log

def plot_html(data, trip_time, durations, filename, channel, spike_count, year):
    # reads oms csv to get start times of fills.
    fill_data = pd.read_csv(f'{year}_oms.csv')
    fill_dict = {}

    # initialise an array to write spike info into.
    spike_info = []
    # Convert 'Start Time' and 'End Time' fill values to Timestamp objects with Europe Timezone
    fill_data['Start Time'] = pd.to_datetime(fill_data['Start Time']).dt.tz_localize('UTC').dt.tz_convert('Europe/Stockholm')
    fill_data['End Time'] = pd.to_datetime(fill_data['End Time']).dt.tz_localize('UTC').dt.tz_convert('Europe/Stockholm')
    # initialize counter to index correct elements of duration for each trip
    counter = 0 
    for i in range(1, len(fill_data) - 1):
        fill_dict[fill_data['Fill'][i]] = {'Start Time':fill_data['Start Time'][i], 'Peak Pileup': round(fill_data['peak_pileup'][i],1)
        }

        # Iterate through each spike in spike_data
        for date in trip_time:
            #print(date)
            fill_start_time = fill_data['Start Time'][i]
            fill_end_time = fill_data['End Time'][i]

            # Check if the spike falls within the time range of the current fill
            if fill_start_time <= date <= fill_end_time:
                # Calculate the time from the start of the current fill that the spike occurred
                time_since_start = (date - fill_start_time)
                # Append information for each spike
                spike_info.append({
                    'SpikeDate': date,
                    'Fill': fill_data['Fill'][i],
                    'TimeSinceStart': time_since_start,
                    'Duration': durations[counter]
                })
                counter += 1
    # Convert the list to a DataFrame
    spike_info_df = pd.DataFrame(spike_info)

    # Save the DataFrame to a CSV file
    spike_info_df.to_csv(f'./trips/{year}/ch {channel} spike_info.csv', index=False)
    fig = px.line(data, x='Date', y='ACTUAL_IMON', title=f'{year} BCM1F HV IMON Plot for Ch {channel}', labels={'ACTUAL_IMON': 'Current [uA]'})
    # write a textbox with the number of spikes in top left corner
    fig.add_annotation(
        xref="paper", yref="paper",
        x=0.02, y=0.98,
        text=f"Number of spikes: {spike_count}",
        showarrow=False,
        font=dict(
            family="Courier New, monospace",
            size=20,
            color="#000000"

            ),
        align="left",
        bordercolor="#c7c7c7",
        borderwidth=2,
        borderpad=4,
        bgcolor="#ff7f0e",
        opacity=0.8
    )

    for fill, fill_info in fill_dict.items():
        start_time = fill_info['Start Time']
        peak_pileup = fill_info['Peak Pileup']
        # Add dashed lines for fill start times
        fig.add_shape(
            dict(
                type="line",
                x0=start_time,
                x1=start_time,
                y0=0,
                y1=max(data['ACTUAL_IMON']),
                line=dict(color="green", width=1, dash="dash"),
            )
        )
        fig.add_annotation(
            dict(
                x=start_time,
                y=max(data['ACTUAL_IMON']),
                text=f"Fill {fill} (PU: {peak_pileup})",
                showarrow=True,
                arrowhead=3,
                arrowcolor="green",
                ax=0,
                ay=-30,
                font=dict(
                    family="Courier New, monospace",
                    size=16,  # Adjust the size as needed
                    color="#000000"  # Adjust the color if needed
                ),
                align="center",
            )
        )

    # add a vertical red line for all the Date values in spike_data
    for date in trip_time:
        fig.add_vline(x=date, line_width=3, line_dash="dash", line_color="red")
    fig.write_html(filename)
    fig.show()

def example():
    channel = int(sys.argv[2])
    year = int(sys.argv[1])
    data = parse_channel(log_file=f'{year}-bcm1f-hv-imon.csv', ch=channel, ch_map=BCM1F_CHANNEL_MAP)
    is_trip = False
    start_time = None
    end_time = None
    trip_count = 0

    # record start time of each trip, and the duration
    durations = []
    trip_time = []

    for index, row in data.iterrows():
        if row['ACTUAL_IMON'] > 40 and not is_trip:
            # Trip started
            is_trip = True
            start_time = row['Date']
            trip_time.append(start_time)
        elif row['ACTUAL_IMON'] <= 30 and is_trip:
            # Trip ended
            is_trip = False
            end_time = row['Date']
            trip_count += 1
            # calculate how long this trip lasted and save it into a list
            duration = end_time - start_time
            durations.append(duration)
            print(f'Trip {trip_count}: Start Time - {start_time}, End Time - {end_time}, Duration - {duration}')

    print(f'Total Trips: {trip_count}')

    # plot the data
    plot_html(data,trip_time,durations,f'./plots/{year}/ch_{channel}.html', channel, trip_count, year)


    # write a csv with channel number, spike count
    with open(f'spike_count_{year}.csv', 'a') as f:
        f.write(f'{channel},{trip_count}\n')

if __name__ == '__main__':
    example()