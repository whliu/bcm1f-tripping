import os
import pandas as pd
import matplotlib.pyplot as plt

# Function to process each CSV file
def process_csv(file_path):
    # Read the CSV file
    df = pd.read_csv(file_path)

    # Convert 'TimeSinceStart' and 'Duration' columns to timedelta objects
    df['TimeSinceStart'] = pd.to_timedelta(df['TimeSinceStart'])
    df['Duration'] = pd.to_timedelta(df['Duration'])

    # Convert timedelta objects to seconds
    time_since_start_seconds = df['TimeSinceStart'].dt.total_seconds().values
    duration_seconds = df['Duration'].dt.total_seconds().values

    # Process the 'Fill' column
    fill_counts = df['Fill'].value_counts().to_dict()

    return time_since_start_seconds, duration_seconds, len(df), fill_counts

# Initialize empty arrays and dictionaries
all_time_since_start = []
all_duration = []
channel_trip_count = {}
fill_trip_count = {}

# Function to update the fill trip count dictionary
def update_fill_trip_count(fill_counts):
    for fill, count in fill_counts.items():
        if fill in fill_trip_count:
            fill_trip_count[fill] += count
        else:
            fill_trip_count[fill] = count

# Process files in the first directory
directory_path = '/localdata/whliu/bcm1f-tripping/trips/2023/'
for filename in os.listdir(directory_path):
    if filename.endswith(".csv"):
        file_path = os.path.join(directory_path, filename)
        channel_number = int(filename.split()[1])

        # Process the CSV file
        time_since_start, duration, trip_count, fill_counts = process_csv(file_path)

        # Append the data and update counts
        all_time_since_start.extend(time_since_start)
        all_duration.extend(duration)
        channel_trip_count[channel_number] = trip_count
        update_fill_trip_count(fill_counts)

# Repeat the process for the second directory
directory_path2 = '/localdata/whliu/bcm1f-tripping/trips/2022/'
for filename in os.listdir(directory_path2):
    if filename.endswith(".csv"):
        file_path = os.path.join(directory_path2, filename)
        channel_number = int(filename.split()[1])

        # Process the CSV file
        time_since_start, duration, trip_count, fill_counts = process_csv(file_path)

        # Append the data and update counts
        all_time_since_start.extend(time_since_start)
        all_duration.extend(duration)
        channel_trip_count[channel_number] += trip_count
        update_fill_trip_count(fill_counts)

# Continue with plotting the histograms and bar graphs as before


# Plot histograms
fig, axs = plt.subplots(4, 1, figsize=(10, 8))

# Histogram for Time Since Start
axs[0].hist(all_time_since_start, bins=100, range=(0,20000), color='blue', alpha=0.7)
axs[0].set_title('Histogram of Time Since Start (below 20,000s)')
axs[0].set_xlabel('Time Since Start (seconds)')
axs[0].set_ylabel('Frequency')


axs[1].hist(all_duration, bins=50, color='green', alpha=0.7)
axs[1].set_title('Histogram of Duration')
axs[1].set_xlabel('Duration (seconds)')
axs[1].set_ylabel('Frequency')


# Histogram for Duration below 5000 seconds
axs[2].hist(all_duration, bins=100, range=(0, 2000), color='green', alpha=0.7)  # Increased number of bins and limited range
axs[2].set_title('Histogram of Duration (below 2000 seconds)')
axs[2].set_xlabel('Duration (seconds)')
axs[2].set_ylabel('Frequency')

# Bar graph for number of rows (trips) per channel
channels = list(channel_trip_count.keys())
trip_counts = list(channel_trip_count.values())
axs[3].bar(channels, trip_counts, color='orange', alpha=0.7)
axs[3].set_title('Number of Trips per Channel')
axs[3].set_xlabel('Channel')
axs[3].set_ylabel('Number of Trips')
axs[3].set_xticks(channels)
axs[3].set_xticklabels(channels, rotation=0)

plt.tight_layout()
plt.savefig('Tripping Analysis V3.pdf')
plt.show()

print(fill_trip_count)