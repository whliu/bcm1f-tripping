import tables as t
import os
import time, pytz
from datetime import datetime
import pylab as py
import numpy as np

convert = lambda x: datetime.fromtimestamp(int(x), pytz.timezone("Europe/Stockholm"))
stringToPOSIX = lambda str: time.mktime(datetime.strptime(str, "%d-%m-%y %H:%M:%S").timetuple())

class Lumitable(t.IsDescription):
	fillnum = t.UInt32Col(shape=(), dflt=0, pos=0)
	runnum = t.UInt32Col(shape=(), dflt=0, pos=1)
	lsnum = t.UInt32Col(shape=(), dflt=0, pos=2)
	nbnum = t.UInt32Col(shape=(), dflt=0, pos=3)
	timestampsec = t.UInt32Col(shape=(), dflt=0, pos=4)
	timestampmsec = t.UInt32Col(shape=(), dflt=0, pos=5)
	totsize = t.UInt32Col(shape=(), dflt=0, pos=6)
	publishnnb = t.UInt8Col(shape=(), dflt=0, pos=7)
	datasourceid = t.UInt8Col(shape=(), dflt=0, pos=8)
	algoid = t.UInt8Col(shape=(), dflt=0, pos=9)
	channelid = t.UInt8Col(shape=(), dflt=0, pos=10)
	payloadtype = t.UInt8Col(shape=(), dflt=0, pos=11)
	calibtag = t.StringCol(itemsize=32, shape=(), dflt='', pos=12)
	avgraw = t.Float32Col(shape=(), dflt=0.0, pos=13)
	avg = t.Float32Col(shape=(), dflt=0.0, pos=14)
	bxraw = t.Float32Col(shape=(3564,), dflt=0.0, pos=15)
	bx = t.Float32Col(shape=(3564,), dflt=0.0, pos=16)
	maskhigh = t.UInt32Col(shape=(), dflt=0, pos=17)
	masklow = t.UInt32Col(shape=(), dflt=0, pos=18)
    

def get_year(fill):
    if fill<5633:
        raise ValueError('Fill number too small, this script only works with 2017 and 2018 data')
    elif fill<6540:
        year = 17
    elif fill<7920:
        year = 18
    elif fill<8500:
        year = 22
    elif fill<9320:
        year = 23
    else:
        year = 24
    return year


def create_outputPath(output_path, year, fill):
    output_path += str(year)
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    output_path += '/'+str(fill)+'/'
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    return output_path


def create_channelMasks(disabled_channels):
    maskhigh = 0; masklow = 0
    for channel in disabled_channels: 
        #masklow
        if(channel==0):
            masklow += 0x1 #Use hexadecimal numbers
        elif(channel==1):
            masklow += 0x2
        elif(channel==2):
            masklow += 0x10
        elif(channel==3):
            masklow += 0x20
        elif(channel==4):
            masklow += 0x100
        elif(channel==5):
            masklow += 0x200
        elif(channel==6):
            masklow += 0x1000
        elif(channel==7):
            masklow += 0x2000
        #maskhigh
        if(channel==8):
            maskhigh += 0x1
        elif(channel==9):
            maskhigh += 0x2
        elif(channel==10):
            maskhigh += 0x10
        elif(channel==11):
            maskhigh += 0x20
        elif(channel==12):
            maskhigh += 0x100
        elif(channel==13):
            maskhigh += 0x200
        elif(channel==14):
            maskhigh += 0x1000
        elif(channel==15):
            maskhigh += 0x2000
    masklow = 0x3333-masklow #You want the active channels, not the disabled ones
    maskhigh = 0x3333-maskhigh
    return masklow, maskhigh



def apply_albedo(bxraw, bxmask, albedo_queue, albedo_queue_length, calc_counter, albedo_model_full, albedo_fraction, calculate_albedo_every, noise, noise_calc_start, noise_calc_end):
    # put data in albedo queue
    albedo_queue.put(bxraw)
    calc_counter +=1
    if calc_counter > calculate_albedo_every:
        calc_counter = 0
        # average everything in albedo queue
        uncorrected = py.zeros(3564)
        queuesize = albedo_queue.qsize()
        for i in range(queuesize):
            tmp = albedo_queue.get()
            uncorrected += tmp
            if albedo_queue.qsize() < albedo_queue_length:
                albedo_queue.put(tmp)
        uncorrected /= queuesize
		# calculate albedo
        corrected = uncorrected.copy()
        for i,m in enumerate(bxmask):
            if m: corrected -= corrected[i]*py.roll(albedo_model_full,i)

		# make fractions
        albedo_fraction[uncorrected!=0] = corrected[uncorrected!=0]/uncorrected[uncorrected!=0]

		# noise to subtract
        noise = corrected[noise_calc_start:noise_calc_end].mean()

    bxraw = bxraw*albedo_fraction - noise
    return bxraw, albedo_queue, albedo_fraction,calc_counter, noise 


def build_average(agghist_list, channel_mask, sig_vis_perChannel, sigvis_avg):
    ch_count = len(agghist_list)
    agghist_sum = py.zeros(3564)
    excludeCh_list = []

    # this is average just for excluding low rate channels 
    for agghist in agghist_list:
        agghist_sum += agghist
    agghist_avg = agghist_sum / ch_count

    # here calibrate and average 
    abort_gap = 3400 
    bxraw_weighted = py.zeros(3564)
    for agghist, channel_id in zip(agghist_list, channel_mask):
        if agghist[:abort_gap].sum() < 0.5*agghist_avg[:abort_gap].sum():   
            ch_count -= 1
            excludeCh_list.append(channel_id)
            print(f"*** < 50% rate found on channel {channel_id} - excluding")
        else:
            # process lumi now
            r0 = 1-(agghist/2**14)
            r0[r0<0] = 0
            mu = -py.log(r0)
            bxraw_weighted += mu / sig_vis_perChannel[channel_id]
            
    bxraw_avg = bxraw_weighted / ch_count * sigvis_avg
    return bxraw_avg, excludeCh_list


# This way of calculating luminosity implements the online algorithm
# https://gitlab.cern.ch/cmsos/bril/-/blob/baseline_kiwi_2/bril/bcm1futcaprocessor/include/bril/bcm1futcaprocessor/Algorithms/ZeroCounting.h#L81
def build_average_online_copy(agghist_list, channel_mask, sigvis_avg, sig_vis_perChannel, bxmask, c0, c2, time):
    # First calculate the reference luminosity without any masking
    ref_lumi = 0
    excludeCh_dict = {}
    ch_count = len(agghist_list)

    for agghist, channel_id in zip(agghist_list, channel_mask):
        r0 = 1-(agghist/2**14)
        r0[r0<0] = 0
        mu = -py.log(r0)
        perChannelLumi_raw = (11246 * mu / sig_vis_perChannel[channel_id]*bxmask).sum()
        ref_lumi += perChannelLumi_raw + c2*perChannelLumi_raw**2 + c0

    # Reference luminosity without channel masking - average of channels
    ref_lumi /= ch_count

    
    bxraw_weighted = py.zeros(3564)
    
    # Now check which channels are < 80% of the reference luminosity
    for agghist, channel_id in zip(agghist_list, channel_mask):
        r0 = 1-(agghist/2**14)
        r0[r0<0] = 0
        mu = -py.log(r0)
        perChannelLumi_raw = (11246 * mu / sig_vis_perChannel[channel_id]*bxmask).sum() # Mask out non colliding bunches
        perChannelLumi = perChannelLumi_raw + c2*perChannelLumi_raw**2 + c0

        # Check if the luminosity for each channel is below 80% of the reference luminosity
        if perChannelLumi > 1.2 * ref_lumi:
            if perChannelLumi > 50:
                # Less than 80% found, mask channel out
                ch_count -= 1
                excludeCh_dict[channel_id] = str(convert(time))
        elif perChannelLumi < 0.8 * ref_lumi:
            if perChannelLumi > 50:
                # Less than 80% found, mask channel out
                ch_count -= 1
                excludeCh_dict[channel_id] = str(convert(time))
        else:
            bxraw_weighted += mu / sig_vis_perChannel[channel_id]

    bxraw_avg = bxraw_weighted / ch_count * sigvis_avg
    return bxraw_avg, excludeCh_dict


def per_channel_lumi(agghist, sig_vis_perChannel, ch):
    # Extract the average rate from histogram - zero counting
    r0 = 1-(agghist/2**14)
    r0[r0<0] = 0
    mu = -py.log(r0)

    # For each channel, divide rate by channel sigma visible
    return mu / sig_vis_perChannel[ch]

def trip_detector(agghist_list, channel_mask, sig_vis_perChannel, sigvis_avg, bxmask, c0, c2, lasttime):
    # First calculate the reference luminosity without any masking
    ref_lumi = 0
    excludeCh_dict = {}
    timestamp_dict = {}
    ch_count = len(agghist_list)
    for agghist, channel_id in zip(agghist_list, channel_mask):
        r0 = 1-(agghist/2**14)
        r0[r0<0] = 0
        mu = -py.log(r0)
        perChannelLumi_raw = (11246 * mu / sig_vis_perChannel[channel_id]*bxmask).sum()
        ref_lumi += perChannelLumi_raw + c2*perChannelLumi_raw**2 + c0

    # Reference luminosity without channel masking - average of channels
    ref_lumi /= ch_count
    for agghist, channel_id in zip(agghist_list, channel_mask):
        r0 = 1-(agghist/2**14)
        r0[r0<0] = 0
        mu = -py.log(r0)
        perChannelLumi_raw = (11246 * mu / sig_vis_perChannel[channel_id]*bxmask).sum() # Mask out non colliding bunches
        perChannelLumi = perChannelLumi_raw + c2*perChannelLumi_raw**2 + c0

        # Check for trips

        if perChannelLumi < 0.8 * ref_lumi:
            ch_count -= 1
            timestamp_dict[channel_id] = lasttime
            excludeCh_dict[channel_id] = str(convert(lasttime))
        elif perChannelLumi > 1.2 * ref_lumi:
            ch_count -= 1
            timestamp_dict[channel_id] = lasttime
            excludeCh_dict[channel_id] = str(convert(lasttime))
    return excludeCh_dict, timestamp_dict

