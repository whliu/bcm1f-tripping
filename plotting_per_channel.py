import matplotlib.pyplot as plt
import reprocess_utils as utils
import pandas as pd 
import tables
from os import listdir
import plotly.express as px
import sys
import numpy as np

pd.options.plotting.backend = "plotly" # for whenever plotly is available 

def read_file(original_filename, reprocessed_filename, col_names):
    print("***** reading: ", original_filename, reprocessed_filename)
    lumi = pd.DataFrame()

    for colname in col_names:
        with tables.open_file(original_filename, 'r') as f:
            lumi_tmp = pd.DataFrame()
            lumi_tmp[colname] = [np.array(row['avg']) for row in f.root[colname]]
            lumi_tmp.index = [row['timestampsec'] for row in f.root[colname]]
            lumi = pd.concat([lumi,lumi_tmp], axis=1)
            lumi = lumi.dropna()

    lumi.index = lumi.index.astype(int)
    lumi.index = lumi.index.to_series().apply(utils.convert)
    return lumi

def read_allFiles(path, col_names):
    lumi_allFiles = pd.DataFrame()
    allFileNames = listdir(original_path)

    for filename in allFileNames:
        lumi_singleFile = read_file(path+filename, out_tablename, col_names)
        lumi_allFiles = pd.concat([lumi_allFiles,lumi_singleFile], axis=0)

    return lumi_allFiles

def getLumi(original_path, reprocessed_path, col_names_original, col_names_reprocessed):
    lumi_original = read_allFiles(original_path, col_names_original)
    lumi_reprocessed = read_allFiles(reprocessed_path, col_names_reprocessed)
    return pd.concat([lumi_original, lumi_reprocessed], axis=1)

# Per channel plots included: python plotting_scripts.py <fill num> -p 
if __name__=='__main__':
    fill = int(sys.argv[1])
    year = utils.get_year(fill) 

    original_path = '/brildata/'+str(year)+'/'+str(fill)+'/'
    reprocessed_path = '/brildpg/23_updates/bcm1futca_reprocess_perChannel/'+str(year)+'/'+str(fill)+'/'

    out_tablename = 'bcm1futcalumi'
    col_names = []
    all_channels = np.arange(1,49)
    excluded_channels = [8, 9, 10, 11, 12, 24, 41, 42, 43, 44, 45, 46, 47, 48] # There is no sigma_vis data for these channels
    channel_mask = [ch for ch in all_channels if ch not in excluded_channels]   

    for ch in channel_mask:
        col_names.append(out_tablename + '_ch' + str(ch))

    col_names_original = [out_tablename]

    plot_data = pd.DataFrame()
    lumi_df = getLumi(original_path, reprocessed_path, col_names_original, col_names)

    # Plot the online lumi, reprocessed lumi and ratios
    plot_data['online lumi - scaled'] = np.array(lumi_df[out_tablename]).astype(np.float32)
    for i, ch in enumerate(channel_mask):
        plot_data['reprocessed CH' + str(ch)] = np.array(lumi_df[col_names[i]]).astype(np.float32)

    plot_data.index = lumi_df.index 

    fig = plot_data.plot(title=f'fill {fill}')
    fig.update_layout(showlegend=True)
    fig.show()

    perChannelPath = f'./output/trips/perChannel/reprocessed_{out_tablename}_{fill}.html'
    print("Saving to " + perChannelPath)
    fig.write_html(perChannelPath)
    
