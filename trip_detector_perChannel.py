import matplotlib.pyplot as plt
import reprocess_utils as utils
import pandas as pd 
import tables as t
from os import listdir
import plotly.express as px
import sys
import numpy as np
import os
from datetime import datetime
import pytz
from typing import Dict, List


pd.options.plotting.backend = "plotly"  # for whenever plotly is available 
convert = lambda x: datetime.fromtimestamp(int(x), pytz.timezone("Europe/Stockholm"))

all_channels = np.arange(1, 49)
fill = int(sys.argv[1])
excluded_channels = [8, 9, 10, 11, 12, 24, 41, 42, 43, 44, 45, 46, 47, 48]  # There is no sigma_vis data for these channels
channel_mask = [ch for ch in all_channels if ch not in excluded_channels]

# Load sigma_vis data which is used to weight the mu values
sig_vis_perChannel = pd.read_csv('updated_sigvis.csv')
sig_vis_perChannel = sig_vis_perChannel.set_index('channelid')
sig_vis_perChannel = sig_vis_perChannel.to_dict()['sig_vis']

# Calculate average sigma_vis
sigvis_avg = 0
for channel in channel_mask:
    sigvis_avg += 1 / sig_vis_perChannel[channel]
sigvis_avg = 1 / (sigvis_avg / len(channel_mask))


def read_file(filename, bxmask, query):
    print("============================================", filename.split("/")[-1])
    try:
        h5in = t.open_file(filename, mode='r')
        intable = h5in.root.bcm1futca_agg_hist
        beam = h5in.root.beam
    except:
        print("File does not contain agghist data - skip")
        return None, False

    lastnb = -1

    output = pd.DataFrame()
    output['timestampsec'] = []

    for ch in channel_mask:
        output['channel_rate_' + str(ch)] = []

    '''
    Each row corresponds to a 4 NB of data for each channel, so
    NB 0, CH1
    NB 0, CH2
        ...
    NB 0, CH48,
    NB 4, CH1,
    NB 4, CH2,
        ...
    '''
    for i, rowin in enumerate(intable.where(query)):
        newrow = {}

        if lastnb == -1:
            lastnb = rowin['nbnum']
            agghist_list = []

        if rowin['nbnum'] != lastnb or i == len(intable) - 1:
            # End of 4 NB reached
            lastnb = rowin['nbnum']

            for agghist, channel_id in zip(agghist_list, channel_mask):
                # Extract mu for this channel only, using zero counting on 4NB of data
                r0 = 1 - (agghist / 2 ** 14)
                r0[r0 < 0] = 0
                mu = -np.log(r0)

                # Mask out non-collidable bunches and add them up
                mu = (mu * bxmask).sum()

                # Scaling according to sigma_vis
                mu_scaled = mu * sigvis_avg / sig_vis_perChannel[channel_id]

                newrow['channel_rate_' + str(channel_id)] = mu_scaled

            newrow['timestampsec'] = rowin['timestampsec']
            output = pd.concat([output, pd.DataFrame([newrow])], axis=0)

            # Reset agghist_list
            agghist_list = []

        agghist_list.append(rowin['agghist'])

    return output, False


def read_allFiles(original_path):
    files = os.listdir(original_path)

    # Load bxmask, which is used to mask out non-collidable bunches
    collTable_file = int(len(files) / 2)
    while collTable_file < len(files):
        try:
            h5 = t.open_file(original_path + files[collTable_file], mode='r')
            bxmask = h5.root.beam[0]['collidable']
            h5.close()
            break
        except:
            print("Problem with beam table - trying next file")
            collTable_file += 1

    # Query only data from channels in channel_mask
    query = ''.join(['(channelid == ' + str(ch) + ') | ' for ch in channel_mask])
    query = '(' + query[:-3] + ') & (algoid == 101)'

    output = pd.DataFrame()

    # The agghist data is spread out over multiple files
    for file in files:
        output_tmp, finished = read_file(original_path + file, bxmask, query)
        output = pd.concat([output, output_tmp], axis=0)
        if finished:
            break

    return output

def get_vdm_query(fill: int, scan_dict: Dict[str, List[List[int]]]) -> str:
    mask_elemements = scan_dict[f"{fill}"]
    if len(mask_elemements) < 1:
        return "timestampsec > 0"
    
    element = mask_elemements[0]
    query = f"((timestampsec >= {element[0]}) & (timestampsec <= {element[1]}))"
    for element in mask_elemements[1:]:
        query += f" | ((timestampsec >= {element[0]}) & (timestampsec <= {element[1]}))"
    return f"~({query})"


# python plotting_channel_rates.py <fill>
# Time format: dd-mm-yy HH:MM:SS

if __name__ == '__main__':
    # Channel mask counting from 1 (BRILDAQ channelid selection)
    all_channels = np.arange(1,49)

    #excluded_channels = [8, 9, 10, 11, 12, 24, 41, 42, 43, 44, 45, 46, 47, 48] # Include 12 later - There is no sigma_vis data for these channels
    excluded_channels = [8, 9, 10, 11, 12, 24, 41, 42, 43, 44, 45, 46, 47, 48]
    channel_mask = [ch for ch in all_channels if ch not in excluded_channels]

    # lumi calibration, parameters as in normtag
    sig_vis_perChannel = pd.read_csv('updated_sigvis.csv')	# pd.read_csv('out_sigvis_vdmNov.csv')
    sig_vis_perChannel = sig_vis_perChannel.set_index('channelid')
    sig_vis_perChannel = sig_vis_perChannel.to_dict()['sig_vis']
    sigvis_avg = 0
    for channel in channel_mask:
        sigvis_avg += 1/sig_vis_perChannel[channel]
    sigvis_avg = 1/(sigvis_avg/len(channel_mask))
    print("Average sigma_vis: " + str(sigvis_avg))
    print("Channels initially excluded: " + str(excluded_channels))

    # Calibration parameters
    c0 = 0                       #offset
    c1 = 11245/sigvis_avg     	 #linear term, usually 11246/sigma_vis
    c2 = 0 						 #quadratic term to correct non-linearity

    year = utils.get_year(fill)
    input_path = '/brildata/'+str(year)+'/'+str(fill)+'/'
    files = os.listdir(input_path)
    scan_dict = {"8637": [], "8639": [[1682160485, 1682160942]], "8642": [[1682223549, 1682224005]], "8644": [[1682264402, 1682264859], [1682286872, 1682287333]], "8645": [[1682333638, 1682334098]], "8654": [], "8675": [[1682632296, 1682632788], [1682639657, 1682641414]], "8685": [[1682755126, 1682755616], [1682766902, 1682768658], [1682781255, 1682781719]], "8686": [[1682792284, 1682792781], [1682802380, 1682804136], [1682822637, 1682823103]], "8690": [[1682857370, 1682857850], [1682858134, 1682859894], [1682860249, 1682861112], [1682861266, 1682861731], [1682861932, 1682862398]], "8691": [[1682887670, 1682888541]], "8692": [], "8695": [[1682949176, 1682950040], [1682967430, 1682968293]], "8696": [[1682985820, 1682986687]], "8701": [], "8723": [[1683303280, 1683304153]], "8724": [], "8725": [[1683360374, 1683361251]], "8728": [], "8729": [[1683412265, 1683413144]], "8730": [[1683484138, 1683484520]], "8731": [[1683505733, 1683506613], [1683515870, 1683516752]], "8736": [[1683603952, 1683604835]], "8738": [[1683663661, 1683664538]], "8739": [[1683690345, 1683690726]], "8741": [[1683746637, 1683746981]], "8746": [[1683798130, 1683798512], [1683810957, 1683811837]], "8750": [], "8754": [[1683902336, 1683902857]], "8771": [[1684060468, 1684060802]], "8773": [[1684129179, 1684129699]], "8775": [[1684189332, 1684189671]], "8778": [[1684241213, 1684241558], [1684243515, 1684243813], [1684244033, 1684244755], [1684248878, 1684249160], [1684249445, 1684250135], [1684252790, 1684253088], [1684253300, 1684253974], [1684257506, 1684257848], [1684254370, 1684257340], [1684261746, 1684262064]], "8782": [[1684291796, 1684292138], [1684313775, 1684314279], [1684324207, 1684324525], [1684324660, 1684324771]], "8784": [[1684362777, 1684363121]], "8786": [[1684413947, 1684414291], [1684467986, 1684468488]], "8794": [[1684518018, 1684518544], [1684518751, 1684519290]], "8796": [[1684552864, 1684553202]], "8804": [[1684641118, 1684641454]], "8807": [[1684680015, 1684680542]], "8811": [[1684724436, 1684724777]], "8816": [[1684778304, 1684778655]], "8817": [[1684800409, 1684800758], [1684842606, 1684843099]], "8821": [[1684885083, 1684885424]], "8822": [[1684949618, 1684949962]], "8850": [], "8853": [[1685492187, 1685492531]], "8858": [[1685624931, 1685625275], [1685633488, 1685633810]], "8860": [[1685657123, 1685657471], [1685692582, 1685692901]], "8863": [[1685722030, 1685722375]], "8865": [[1685751801, 1685752149], [1685788963, 1685789282]], "8866": [[1685796590, 1685796938]], "8870": [[1685836210, 1685836557], [1685871930, 1685872250]], "8872": [[1685888079, 1685888430]], "8873": [[1685906680, 1685907031], [1685950975, 1685951299]], "8877": [[1685970617, 1685970963]], "8880": [[1686023250, 1686023766], [1686080019, 1686080326]], "8882": [[1686090947, 1686091281]], "8885": [[1686184129, 1686184463], [1686202752, 1686203234]], "8887": [[1686259758, 1686260014], [1686259919, 1686260259], [1686273199, 1686273511]], "8891": [], "8894": [[1686381211, 1686381544]], "8895": [[1686441842, 1686442362]], "8896": [[1686482795, 1686483133], [1686523079, 1686523419]], "8901": [[1686623388, 1686623909]], "8997": [[1687948637, 1687949318], [1687964604, 1687964659], [1687969033, 1687969713], [1687970080, 1687970503], [1687971856, 1687972770]], "8999": [[1687983904, 1687984582], [1687985139, 1687985563], [1687986012, 1687987145], [1687987691, 1687988828], [1687986012, 1687988828], [1687989143, 1687990219], [1687990540, 1687991615], [1687989143, 1687991615], [1687992304, 1687993602], [1687993818, 1687995144], [1687992304, 1687995144], [1687995410, 1687996730], [1687996953, 1687998246], [1687995410, 1687998246], [1687998655, 1687999791], [1688000010, 1688001142], [1687998655, 1688001142], [1688001388, 1688002112], [1688016285, 1688016708], [1688017020, 1688018150], [1688018541, 1688019676], [1688017020, 1688019676], [1688020183, 1688021331], [1688021796, 1688022948], [1688020183, 1688022948], [1688023362, 1688024500], [1688024768, 1688025900], [1688023362, 1688025900], [1688026319, 1688027397], [1688027676, 1688028751], [1688026319, 1688028751], [1688029114, 1688030244], [1688030453, 1688031591], [1688031955, 1688032725], [1688029114, 1688031591], [1688032717, 1688033441], [1688034686, 1688035817], [1688036023, 1688037160], [1688034686, 1688037160], [1688056908, 1688057958], [1688058526, 1688059580], [1688059791, 1688061056], [1688061267, 1688062492], [1688062792, 1688064026], [1688064289, 1688065563], [1688065871, 1688066697], [1688066688, 1688067711]], "9007": [[1688149442, 1688150928], [1688144765, 1688148891]], "9014": [], "9016": [[1688214910, 1688215238]], "9017": [[1688229984, 1688230313]], "9019": [[1688284441, 1688284744]], "9022": [[1688315144, 1688315650]], "9023": [[1688342615, 1688343122], [1688351143, 1688352632], [1688353462, 1688354879]], "9029": [[1688441675, 1688445898], [1688446258, 1688447336]], "9031": [[1688490547, 1688490873], [1688537057, 1688537358]], "9035": [[1688555070, 1688555400]], "9036": [[1688606421, 1688606756]], "9043": [[1688728231, 1688728563]], "9044": [[1688789326, 1688789846]], "9045": [[1688845288, 1688845622]], "9046": [[1688900298, 1688900638]], "9049": [[1688929730, 1688930063]], "9050": [[1688992209, 1688992548]], "9055": [[1689045602, 1689045937]], "9056": [[1689060052, 1689060387]], "9057": [[1689068306, 1689068640]], "9059": [[1689119726, 1689120061]], "9062": [[1689165489, 1689165823]], "9063": [[1689183544, 1689183879]], "9066": [[1689321067, 1689321405]], "9067": [[1689383446, 1689383780]], "9068": [[1689398048, 1689398381]], "9070": [[1689441732, 1689442073]], "9072": [[1689494453, 1689494788]], "9073": [[1689547928, 1689548412]]}

    vdm_query = get_vdm_query(fill, scan_dict)

    # Create data query condition
    query = ''.join(['(channelid == '+str(ch)+ ') | ' for ch in channel_mask])
    query = '('+query[:-3]+') & (algoid == 101)' 
    query = f'({query}) & ({vdm_query})'

    # Get bunch mask from middle file (hopefully containing collisions)
    collTable_file = int(len(files)/2)
    while collTable_file < len(files):
        try:
            h5 = t.open_file(input_path+files[collTable_file],mode='r')
            bxmask = h5.root.beam[0]['collidable']
            h5.close()
            break
        except:
            print("Problem with beam table - trying next file")
            collTable_file += 1


    #prepare albedo queue
    calc_counter = 0
    noise = 0
    first_time_in_fill = None
    for file in files:
        print("============================================",file)
        # Input file
        try:
            h5in = t.open_file(input_path+file,mode='r')
            intable = h5in.root.bcm1futca_agg_hist
        except:
            print("WARNING: File does not include agghist data - skip")
            continue

        # Output file

        # Channels excluded because of rate drop (below 80% or above 120% of average)
        excludeCh_dict_start = {}
        excludeCh_dict_end = {}
        timestamp_start = {}
        timestamp_end = {}
        lastnb = -1

        table_len = len(list(intable.where(query))) #if size ok for memory to make into list?

        for i, rowin in enumerate(intable.where(query)):
            if lastnb == -1:
                lastrun = rowin['runnum']
                lastls = rowin['lsnum']
                lastnb = rowin['nbnum']
                lasttime = rowin['timestampsec']
                lasttimems = rowin['timestampmsec']
                agghist_list = []

                # find the start time of the fill
                if first_time_in_fill is None:
                    first_time_in_fill = rowin['timestampsec']

            if lastnb != rowin['nbnum'] or i == table_len-1:
                # Necessary to include the very last data point in each file
                if i == table_len-1:
                    agghist_list.append(rowin['agghist'])
                    
                # build agghist avg - check for HV trips, calibrate 
                #bxraw, exclude_rateDrop = utils.build_average(agghist_list, channel_mask, sig_vis_perChannel, sigvis_avg, lasttime)
                exclude_rateDrop, timestamp_dict = utils.trip_detector(agghist_list, channel_mask, sig_vis_perChannel, sigvis_avg, bxmask, c0, c2, lasttime)
                excluded_channels_updated = excluded_channels + list(exclude_rateDrop.keys())
                # Fills before 8882 need to be shifted by 1, fixed after.

                # Add newly excluded channels to excludeCh_dict
                excludeCh_dict_end.update(exclude_rateDrop) # Keeps last rate drop occurrence
                exclude_rateDrop.update(excludeCh_dict_start) # Keeps first rate drop occurrence for each channel
                excludeCh_dict_start = exclude_rateDrop.copy()
                
                timestamp_end.update(timestamp_dict)
                timestamp_dict.update(timestamp_start)
                timestamp_start = timestamp_dict.copy()
                # calibrate and sum up for total

                #reinitialize
                lastrun = rowin['runnum']
                lastls = rowin['lsnum']
                lastnb = rowin['nbnum']
                lasttime = rowin['timestampsec']
                lasttimems = rowin['timestampmsec']
                agghist_list = []

            agghist_list.append(rowin['agghist'])
        
    # Save excluded channels to a text file
    excluded_channels_file = f'./logs/excluded_channels_fill{fill}.txt'
    time_diff_start = {channel_id: (int(time) - int(first_time_in_fill)) for channel_id, time in timestamp_start.items()}
  
    # Initialize dictionary to store trip durations
    trip_durations = {}

    for channel_id, start_time in timestamp_start.items():
        # Ensure the channel is present in both timestamp_start and timestamp_end
        if channel_id in timestamp_end:
            # Calculate the duration of the trip
            duration_seconds = int(timestamp_end[channel_id]) - int(start_time)
            # Add the duration to the dictionary
            trip_durations[channel_id] = duration_seconds
            
start = convert(first_time_in_fill)
print(start)

with open(excluded_channels_file, 'w') as file:
    file.write("Channels that were at some point excluded because of rate drop:\n")
    # Check if the time_diff_start for a channel is not 0 before excluding
    for channel_id in excludeCh_dict_start:
        if time_diff_start.get(channel_id, 0) != 0:
            file.write(f"Channel {channel_id}: First occurrence - {excludeCh_dict_start[channel_id]}, Last occurrence - {excludeCh_dict_end.get(channel_id, 'No last occurrence')}, Duration - {trip_durations.get(channel_id, 'No duration')}, Time from start - {time_diff_start.get(channel_id, 'No time from start')}\n")
    print(f"Excluded channels log saved to: {excluded_channels_file}")
    print("Channels that were excluded due to trips: First occurrence ", excludeCh_dict_start, " Last occurrence ", excludeCh_dict_end)
    print(f"Start time of fill {start_time}")

    def convert_duration(seconds):
        hours, remainder = divmod(seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        return hours, minutes, seconds

    for channel in trip_durations.keys() & time_diff_start.keys():
        trip_duration_hours, trip_duration_minutes, trip_duration_seconds = convert_duration(trip_durations[channel])
        time_diff_hours, time_diff_minutes, time_diff_seconds = convert_duration(time_diff_start[channel])

        print(f"For channel {channel}, the trip duration is {trip_duration_hours} hours, {trip_duration_minutes} minutes, and {trip_duration_seconds} seconds.")
        print(f"This trip occurred {time_diff_hours} hours, {time_diff_minutes} minutes, and {time_diff_seconds} seconds into the fill.")


quit()
original_path = '/brildata/' + str(year) + '/' + str(fill) + '/'
plot_data = pd.DataFrame()

data = read_allFiles(original_path)
data['timestampsec'] = data['timestampsec'].astype(int)
data['timestampsec'] = data['timestampsec'].apply(utils.convert)
data.set_index('timestampsec', inplace=True)

# Plot the channel rates
for ch in channel_mask:
    plot_data['channel ' + str(ch)] = data['channel_rate_' + str(ch)]

fig = plot_data.plot(title=f'Channel rates for fill {fill}')
fig.update_layout(showlegend=True)
fig.show()
outputPath = f'./output/trips/channel_rates_{fill}.html'
print("Saving to ", outputPath)
fig.write_html(outputPath)
