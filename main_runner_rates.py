import pandas as pd 
from subprocess import Popen, PIPE
import multiprocessing


def perFill(script_name, fill, path_logs):
    # To run per channel plotting_scripts, add "true" to the list of arguments
    p = Popen(['python', script_name, str(fill)], stdin=PIPE, stdout=PIPE, stderr=PIPE) 
    if fill % 20 == 0:
        print(f'processing fill {fill}')

    result = p.communicate()# b"input data that is passed to subprocess' stdin")
    rc = p.returncode
    #print(f'fill {fill} returned {rc}')
    for name, data in zip(['out','err'], result):
        if name == 'out':
            print(fill,',', data.decode("utf-8"))
        file = open(path_logs+f'{name}_{script_name.split(".")[0]}_{fill}.txt', 'w+')
        file.write(data.decode("utf-8"))
        file.close()

def getFillList(nominalRunStart, nominalRunEnd):
    # replace with /brildata/plt/fills/2023_OMS.csv
    stableBeams = pd.read_csv('/brildata/plt/fills/2023_OMS.csv')
    # names of columns are a bit different - removed OMS_
    stableBeams = stableBeams[stableBeams['stable_beams']==True]
    stableBeams = stableBeams[stableBeams['fill_number'] > nominalRunStart]
    stableBeams = stableBeams[stableBeams['fill_number'] < nominalRunEnd]
    #recorded_lumi = stableBeams['recorded_lumi']
    #delivered_lumi = stableBeams['delivered_lumi']
    stableBeams_fills = stableBeams['fill_number'].tolist()
    print('# all fills to process:', len(stableBeams_fills))
    return stableBeams_fills


if __name__=='__main__':
    # config the start of 2023 run
    nominalRunStart = 9387 #8637 # 8108 # 7920 - for vme, 8108 for 08.08 with all channels configuration 
    nominalRunEnd = 9538 #9073
    path_logs = './logs/errors' # 
    script_name = 'plot_rates.py' # 'plot_perFill.py'  # 
    max_jobs = 8
    fills_to_skip = [] # 8731 has bad file, 8128 bad?, 8332 no beam table, 8293 utca data missing

    # exclude some fills
    stableBeams_fills = getFillList(nominalRunStart, nominalRunEnd)
    fill_list = [9387, 9389, 9442, 9443, 9444, 9473, 9474, 9475, 9476, 9479, 9483, 9485, 9496, 9497, 9499, 9509, 9510, 9512, 9514, 9517, 9518, 9519, 9520, 9521, 9523, 9525, 9529, 9530, 9537, 9539]

    #fill_list = [f for f in stableBeams_fills if f not in fills_to_skip]
    print(fill_list)
    # filter fills
    # fill_list = [f for f in fill_list if f < 8214  or f > 8225] # excluding fill when utca data missing 
    # fill_list = [f for f in fill_list if f < 8126]

    # prepare input
    input_items = [(script_name, fill, path_logs) for fill in  fill_list]

    # run asynchronously 
    pool = multiprocessing.Pool(max_jobs)
    async_result = pool.starmap_async(perFill, input_items)
    async_result.wait()
    pool.close()
    pool.join()

    results = async_result.get()
    for r in results:
        print(r)



