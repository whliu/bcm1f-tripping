# BCM1F Tripping

This repo gives you the tools to plot the current and rates per channel for different fills across 2022,2023,2024. Here is how to use it.

## Different Input Files

the OMS.csv files gives you the information on each of the fills during that year - including the peak PU and start time for each fill. This is used to plot the lines when a fill starts.

The -bcm1f-hv-imon.csv files are current files from the CAEN system. They can be extracted for each channel from this website: https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_services/brilinfo

`findVdM.py` extracts the times of the emittance scans throughout the year. I used it while trying to develop an anomaly detection algorithm to remove trips (for this you need to remove the emittance scans from fills). 

## Plot Rates
To check the per channel rates for each fill - you plot_rates.py. It creates them in interactive plotly format.

## Plot Current
This you use the `current_tripping.py` script. It takes two inputs, which have been detailed above. It creates plotly plots with the current over the year. 

You can analyse the statistics of these trips using `analyse_trips.py`. Currently I just check the number of trips per channel, and their duration and time from start. 